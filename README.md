# apivn : api visionature pour faune-bretagne

Ce projet est basé sur le projet Client_API_VN https://framagit.org/lpo/Client_API_VN

L'objectif est d'obtenir un environnement Docker sous Windows 10.

Ce projet comporte plusieurs conteneurs :
* transfer_vn : le client API Visionature;
** à partir de l'image dthonon/client-api-vn;
** ajout d'un dossier /root
* postgis : base de données PostgreSQL avec Postgis;
** à partir de l'image postgis/postgis
** initialisation de la base
* adminer : application Web d'interface avec la base de données
** à partir de l'image adminer
